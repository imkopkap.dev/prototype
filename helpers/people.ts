import moment from 'moment'
import { IPassengerItem, IDailyItem } from '~/contracts'

/**
 * Get Group Of People By Hour
 * @param passenger
 */
export const groupPeopleByHour = (passengers: IPassengerItem[]): IDailyItem[] => {
  const hash = Object.create({})
  const grouped = []

  for (const passenger of passengers) {
    const key = moment(passenger.checkInTime).format('HH') + ':00'

    if (!hash[key]) {
      hash[key] = { time: key, total: 0 }
      grouped.push(hash[key])
    }

    hash[key].total += 1
  }

  grouped.sort(function (a, b) {
    return a.time.localeCompare(b.time)
  })

  return grouped.map((group) => ({ time: group.time, total: group.total })) || []
}

/**
 * Get From Grouped People From GroupPeopleByHour
 * @param groups
 */
// export const getOnlyTimeFromGroupPeopleByHour = (groups) => {
//   return groups.map((group) => group.x)
// }

/**
 * Get passenger by start time and end time
 * @param passenger
 * @param zoneName
 */
// export const filterPeopleByZoneName = (passenger, zoneName) => {
//   return passenger.filter((person) => person.zone.name === zoneName)
// }

/**
 * Get passenger by start time and end time
 * @param passenger
 * @param date
 * @param startTime
 * @param endTime
 */
// export const filterPeopleByStartTimeAndEndTime = (
//   passenger,
//   date,
//   startTime,
//   endTime
// ) => {
//   if (date && startTime && endTime) {
//     const st = momentParser(date, startTime)
//     const et = momentParser(date, endTime)

//     return passenger.filter((person) => {
//       const checkedInTime = moment(person.checkedInAt)

//       return (
//         checkedInTime.diff(st, 'minutes') >= 0 &&
//         checkedInTime.diff(et, 'minutes') <= 0
//       )
//     })
//   }

//   return passenger
// }
