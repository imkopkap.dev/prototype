import { IFloorItem, IAreaItem, IUnitItem, IPassengerItem } from '~/contracts'

/**
* Given a list of floors and a floor ID, return a list of passengers on that floor
* @param {IFloorItem[]} floors - The floors array from the state.
* @returns An array of passenger objects.
*/
export const getAllPeopleHelper = (floors: IFloorItem[]): IPassengerItem[] => {
  let passengers: IPassengerItem[] = []

  for (const floor of floors) {
    for (const area of floor.areas) {
      for (const unit of area.units) {
        passengers = [...passengers, ...unit.passengers]
      }
    }
  }

  return passengers
}

/**
* Given a list of floors and a floor ID, return a list of passengers on that floor
* @param {IFloorItem[]} floors - The floors array from the state.
* @returns An array of passenger objects.
*/
export const getAllAreaHelper = (floors: IFloorItem[]): IAreaItem[] => {
  let areas: IAreaItem[] = []

  for (const floor of floors) {
    for (const area of floor.areas) {
      areas = [...areas, area]
    }
  }

  return areas
}

/**
* Given a list of floors and a floor ID, return a list of passengers on that floor
* @param {IFloorItem[]} floors - The floors array from the state.
* @returns An array of passenger objects.
*/
export const getAllUnitHelper = (floors: IFloorItem[]): IUnitItem[] => {
  let units: IUnitItem[] = []

  for (const floor of floors) {
    for (const area of floor.areas) {
      for (const unit of area.units) {
        units = [...units, unit]
      }
    }
  }

  return units
}

/**
 * Given a list of floors, return a list of people in the area
 * @param {IFloorItem[]} floors - An array of floors.
 */
 export const getFloorHelper = (floors: IFloorItem[], floorId: string): IFloorItem | undefined => {
  const floor = floors.find(floor => floor.id === floorId)
  return floor
}

/**
 * Given a list of floors and a floorId, return the area with the given areaId
 * @param {IFloorItem[]} floors - The list of floors in the building.
 * @param {string} areaId - The id of the area you want to get.
 * @returns An area item.
 */
export const getAreaHelper = (floors: IFloorItem[], areaId: string): IAreaItem | undefined => {
  for (const floor of floors) {
    for (const area of floor.areas) {
      if (area.id === areaId) {
        return area
      }
    }
  }

  return undefined
}

/**
 * Given a list of floors and a floorId, return the area with the given areaId
 * @param {IFloorItem[]} floors - The list of floors in the building.
 * @param {string} areaId - The id of the area you want to get.
 * @returns An area item.
 */
export const getAreaByNameHelper = (floors: IFloorItem[], areaName: string): IAreaItem | undefined => {
  for (const floor of floors) {
    for (const area of floor.areas) {
      if (area.name === areaName) {
        return area
      }
    }
  }

  return undefined
}

/**
 * Given a list of floors, a floor id, an area id, and a unit id, return the unit item
 * @param {IFloorItem[]} floors - The list of floors.
 * @param {string} unitId - The id of the unit you want to get.
 * @returns The unit object
 */
export const getUnitHelper = (floors: IFloorItem[], unitId: string): IUnitItem | undefined => {
  for (const floor of floors) {
    for (const area of floor.areas) {
      for (const unit of area.units) {
        if (unit.id === unitId) {
          return unit
        }
      }
    }
  }

  return undefined
}

 /**
  * Given a list of floors and a floor ID, return a list of passengers on that floor
  * @param {IFloorItem[]} floors - The floors array from the state.
  * @param {string} floorId - string
  * @returns An array of passenger objects.
  */
export const getPeopleInFloorHelper = (floors: IFloorItem[], floorId: string): IPassengerItem[] => {
  let passengers: IPassengerItem[] = []

  const floor = floors.find(floor => floor.id === floorId)

  if (floor) {
    for (const area of floor.areas) {
      for (const unit of area.units) {
        passengers = [...passengers, ...unit.passengers]
      }
    }
  }

  return passengers
}

/**
 * Given a floor, area, and list of floors, return a list of passengers in that area
 * @param {IFloorItem[]} floors - The floors array from the store.
 * @param {string} areaId - The ID of the area that you want to get the passengers from.
 * @returns An array of passengers.
 */
export const getPeopleInAreaHelper = (floors: IFloorItem[], areaId: string): IPassengerItem[] => {
  let passengers: IPassengerItem[] = []

  for (const floor of floors) {
    for (const area of floor.areas) {
      if (area.id === areaId) {
        for (const unit of area.units) {
          passengers = [...passengers, ...unit.passengers]
        }

        return passengers
      }
    }
  }

  return []
}

/**
 * Given a list of floors, a floor id, an area id, and a unit id, return a list of passengers in that
 * unit
 * @param {IFloorItem[]} floors - The floors array from the state.
 * @param {string} unitId - The ID of the unit you want to get the passengers from.
 * @returns An array of `IPassengerItem`s.
 */
 export const getPeopleInUnitHelper = (floors: IFloorItem[], unitId: string): IPassengerItem[] => {
  for (const floor of floors) {
    for (const area of floor.areas) {
      for (const unit of area.units) {
        if (unit.id === unitId) {
          return unit.passengers
        }
      }
    }
  }

  return []
}

/**
 * Given a list of floors, find the passenger with the given id
 * @param {IFloorItem[]} floors - An array of floors.
 * @param {string} passengerId - string
 * @returns An array of all the passengers in the building.
 */
 export const getPassengerHelper = (floors: IFloorItem[], passengerId: string): IPassengerItem | undefined => {
  for (const floor of floors) {
    for (const area of floor.areas) {
      for (const unit of area.units) {
        for (const passenger of unit.passengers) {
          if (passenger.id === passengerId) {
            return passenger
          }
        }
      }
    }
  }

  return undefined
}

/**
 * Given a list of passengers, return a list of passengers that match the search keyword
 * @param {IPassengerItem[]} passengers - an array of passenger objects
 * @param {string} keyword - string
 * @returns An array of objects that match the search criteria.
 */
export const searchKeyword = (passengers: IPassengerItem[], keyword: string): IPassengerItem[] => {
  return passengers.filter((passenger: IPassengerItem) => {
    return Object.keys(passenger).some((key: string) => {
      return passenger[key as keyof IPassengerItem]?.toString().includes(keyword)
    })
  })
}