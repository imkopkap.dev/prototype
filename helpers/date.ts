import moment from 'moment'

export const momentParser = (date: string, time: string) => {
  if (date && time) {
    const isoDate = moment(`${date} ${time}`).toISOString()
    return moment(isoDate)
  }

  return null
}
