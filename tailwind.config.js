module.exports = {
  mode: 'jit',
  purge: {
    content: [
      './components/**/*.{vue,js}',
      './modules/**/*.{vue,js}',
      './layouts/**/*.vue',
      './pages/**/*.vue',
      './plugins/**/*.{js,ts}',
      './nuxt.config.{js,ts}',
    ],
  },
  darkMode: false,
  theme: {
    extend: {
      colors: {
        blue: {
          default: '#1E5BF6',
          light: '#F5F6FA',
          soft: '#F5F7FC',
        },
        red: {
          default: '#DD3539',
        },
        green: {
          default: '#66D2A1',
        },
        grey: {
          default: '#E9ECF2',
          light: '#D8DFE8',
        },
        black: {
          default: '#000000',
          light: '#878CA6',
        },
      },
    },
  },
  variants: {
    extend: {
      opacity: ['group-hover'],
    },
  },
  plugins: [require('@tailwindcss/line-clamp')],
}
