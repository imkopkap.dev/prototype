import axios from 'axios'

export default axios.create({
  baseURL: 'http://10.34.102.72:8888',
  timeout: 10000,
})
