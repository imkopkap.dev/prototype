const dev = 'http://localhost:3001'
// const prod = 'http://10.34.102.72:8888'

export const baseURL = dev
export const buildingId = '44687dc7295649a2b0055a38dd6f830e'

export const routes = {
  getUnits: `${baseURL}/api/Buildings/${buildingId}/GetRooms`,
  getZones: `${baseURL}/api/Buildings/${buildingId}/GetFloorAreas`,
  getFloorPlans: `${baseURL}/api/Buildings/${buildingId}/GetFloorPlans`,
  getPassengers: (startDate, endDate) =>
    `${baseURL}/api/Buildings/${buildingId}/GetPassengers/${startDate}/to/${endDate}`,
}
