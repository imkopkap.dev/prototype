import { IBuildingItem } from '~/contracts'
import axios from '~/config/axios'

/**
 * Interfaces
 */
interface IRetrievePassengersRequest {
  buildingId: string
  startTime: string
  endTime: string
}

interface IRetrievePassengersResponse {
  isSuccess: boolean
  errorMessage?: string
  item?: IBuildingItem
}

/**
 * Method: Fetcher
 */
export default async (req: IRetrievePassengersRequest): Promise<IRetrievePassengersResponse> => {
  const endpoint = `/api/buildings/${req.buildingId}/${req.startTime}/to/${req.endTime}`

  try {
    const { data = [] } = await axios.get(endpoint)

    return {
      isSuccess: true,
      item: {
        id: req.buildingId,
        floors: data
      },
    }
  } catch (e) {
    return {
      isSuccess: false,
      errorMessage: e.message,
    }
  }
}
