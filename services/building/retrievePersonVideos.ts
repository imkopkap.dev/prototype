import { IVideoItem } from '~/contracts'
import axios from '~/config/axios'

/**
 * Interfaces
 */
interface IRetrievePersonVideosRequest {
  passengerId: string
  cameraId: string
  startTime: string
  endTime: string
}

interface IRetrievePersonVideosResponse {
  isSuccess: boolean
  errorMessage?: string 
  items?: IVideoItem[]
}

/**
 * Method: Fetcher
 */
export default async (req: IRetrievePersonVideosRequest): Promise<IRetrievePersonVideosResponse> => {
  const endpoint = `/api/CameraRecords/Get/${req.cameraId}/From/${req.startTime}/to/${req.endTime}/user/${req.passengerId}`

  try {
    const { data = [] as unknown as IVideoItem[] } = await axios.get(endpoint)

    return {
      isSuccess: true,
      items: data,
    }
  } catch (e: any) {
    return {
      isSuccess: false,
      errorMessage: e.message,
    }
  }
}
