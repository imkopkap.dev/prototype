import { IVideoItem } from '~/contracts'
import axios from '~/config/axios'

/**
 * Interfaces
 */
interface IRetrieveAreaVideosRequest {
  cameraId: string
  startTime: string
  endTime: string
}

interface IRetrieveAreaVideosResponse {
  isSuccess: boolean
  errorMessage?: string 
  items?: IVideoItem[]
}

/**
 * Method: Fetcher
 */
export default async (req: IRetrieveAreaVideosRequest): Promise<IRetrieveAreaVideosResponse> => {
  const endpoint = `/api/CameraRecords/Get/${req.cameraId}/From/${req.startTime}/to/${req.endTime}`

  try {
    const { data = [] as unknown as IVideoItem[] } = await axios.get(endpoint)

    return {
      isSuccess: true,
      items: data,
    }
  } catch (e: any) {
    return {
      isSuccess: false,
      errorMessage: e.message,
    }
  }
}
