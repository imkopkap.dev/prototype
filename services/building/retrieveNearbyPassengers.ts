import { INearbyPassengerItem } from '~/contracts'
import axios from '~/config/axios'

/**
 * Interfaces
 */
interface IRetrieveNearbyPassengersRequest {
  buildingId: string
  startTime: string
  endTime: string
  passengerId: string
  page: number
  limit: number
}

interface IRetrieveNearbyPassengersResponse {
  isSuccess: boolean
  errorMessage?: string 
  total: number
  status: 0 | 1
  items: INearbyPassengerItem[]
}

/**
 * Method: Fetcher
 */
export default async (req: IRetrieveNearbyPassengersRequest): Promise<IRetrieveNearbyPassengersResponse> => {
  const endpoint = `/api/buildings/${req.buildingId}/${req.startTime}/to/${req.endTime}/passengers/${req.passengerId}/nearby?page=${req.page}&limit=${req.limit}`

  try {
    const { data } = await axios.get(endpoint)

    return {
      isSuccess: true,
      total: data.count,
      status: data.status,
      items: data.result,
    }
  } catch (e: any) {
    return {
      isSuccess: false,
      errorMessage: e.message,
      total: 0,
      status: 0,
      items: []
    }
  }
}
