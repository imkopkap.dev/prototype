import { IPassengerItem } from '~/contracts'
import axios from '~/config/axios'

/**
 * Interfaces
 */
interface ILookupPersonsRequest {
  image: File
}

interface ILookupPersonsResponse {
  isSuccess: boolean
  errorMessage?: string 
  items?: IPassengerItem[]
}

/**
 * Method: Fetcher
 */
export default async (req: ILookupPersonsRequest): Promise<ILookupPersonsResponse> => {
  const endpoint = `/api/global/lookup`

  const payload = new FormData()
  payload.append('image', req.image)

  const config = {
    headers: { 'content-type': 'multipart/form-data' }
  }

  try {
    const { data = [] as unknown as IPassengerItem[] } = await axios.post(endpoint, payload, config)

    return {
      isSuccess: true,
      items: data,
    }
  } catch (e: any) {
    return {
      isSuccess: false,
      errorMessage: e.message,
    }
  }
}
