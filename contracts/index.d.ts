/**
 * Represents a book.
 */
export interface IPassengerHistoryItem {
  id: string
  globalId: string
  profilePhoto: string
  floorName: string
  areaName: string
  unitName: string
  checkInDate: string
  checkInTime: string
  checkOutTime: string
}

/**
 * Represents a book.
 */
export interface IPassengerItem {
  id: string
  displayName: string
  checkInTime: string
  checkOutTime: string
  temperature: number
  profilePhoto: string
  floorName: string
  areaName: string
  unitName: string
  unitX: number
  unitY: number
  histories: IPassengerHistoryItem[]
}

/**
 * Represents a book.
 */
export interface IUnitItem {
  id: string
  name: string
  totalPassengers: number
  passengers: IPassengerItem[]
}

/**
 * Represents a book.
 */
export interface IAreaItem {
  id: string
  name: string
  totalPassengers: number
  cameraIds: string[]
  units: IUnitItem[]
}

/*
 * Represents a book.
 */
export interface IFloorItem {
  id: string
  name: string
  totalPassengers: number
  areas: IAreaItem[]
}

/**
 * Represents a book.
 */
export interface IBuildingItem {
  id: string
  floors: IFloorItem[]
}

/**
 * Represents a book.
 */
export interface IDailyItem {
  time: string
  total: number
}

/**
 * Represents a book.
 */
 export interface IFloorPopulationSummaryItem {
  name: string
  percentage: number
  totalPassengers: number
}

/**
 * Represents a book.
 */
 export interface IUnitTemperatureItem {
  id: string
  name: string 
  total: number
  passengers: IPassengerItem[]
}

/**
 * Represents a book.
 */
 export interface IAreaTemperatureItem {
  id: string
  name: string 
  total: number
  units: IUnitTemperatureItem[]
}

/**
 * Represents a book.
 */
export interface IFloorTemperatureItem {
  id: string
  name: string 
  total: number
  areas: IAreaTemperatureItem[]
}

/**
 * Represents a book.
 */
export interface IVideoItem {
  id: string
  videoId: number
  cameraId: string
  startingTime: string
  endingTime: string
  sourceVideoUrl: string
  bbsVideoUrl: string
}

/**
 * Represents a book.
 */
 export interface INearbyPassengerItem {
  id: string
  temporalId: string
  facePreview: string
  displayName: string
  areaName: string
  unitName: string
  nearByDate: string
  startTime: string
  endTime: string
}