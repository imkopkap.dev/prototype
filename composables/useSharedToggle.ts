import { reactive, toRefs } from '@nuxtjs/composition-api'

export default function (isToggleActive: Boolean = false) {
  /**
   * Reactive state
   */
  const state = reactive({
    isToggleActive,
  })

  /**
   * Method: open dialog
   */
  const openToggle = () => {
    state.isToggleActive = true
  }

  /**
   * Method: close dialog
   */
  const closeToggle = () => {
    state.isToggleActive = false
  }

  /**
   * Method: close dialog
   */
  const toggle = () => {
    state.isToggleActive = !state.isToggleActive
  }

  /**
   * Export
   */
  return {
    ...toRefs(state),
    openToggle,
    closeToggle,
    toggle,
  }
}
