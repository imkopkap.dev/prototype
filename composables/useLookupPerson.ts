import { reactive, toRefs } from '@nuxtjs/composition-api'
import { IPassengerItem } from '~/contracts'
import lookup from '~/services/building/lookupPersons'

export default function () {
  /**
   * Reactive state
   */
  const state = reactive({
    isLookupPersonsFetching: true as boolean,
    persons: [] as IPassengerItem[],
    errorLookupPersonsFetching: undefined as string | undefined
  })

  /**
   * Method: Fetch channels
   */
  const lookupPerson = async (image: File) => {
    state.isLookupPersonsFetching = true

    const { isSuccess, items, errorMessage } = await lookup({ image })

    if (isSuccess && items) {
      state.persons = items
    } else {
      state.errorLookupPersonsFetching = errorMessage
    }

    state.isLookupPersonsFetching = false
  }

  /**
   * Export
   */
  return {
    ...toRefs(state),
    lookupPerson,
  }
}
