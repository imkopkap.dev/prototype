import { computed, reactive, toRefs } from '@nuxtjs/composition-api'
import { INearbyPassengerItem } from '~/contracts'
import retrieveNearbyPassengers from '~/services/building/retrieveNearbyPassengers'

export default function () {
  /**
   * Reactive state
   */
  const state = reactive({
    isNearbyFetching: false as boolean,
    nearby: [] as INearbyPassengerItem[],
    total: 100 as number,
    status: -1 as -1 | 0 | 1,
    errorFetchNearby: undefined as string | undefined
  })

  /**
   * Computed state
   */
  const isNearbyProcessingStatus = computed(() => state.status === 0)
  const isNearbyCompletedStatus = computed(() => state.status === 1)

  /**
   * Method: Fetch channels
   */
  const fetchNearby = async (buildingId: string, startTime: string, endTime: string, passengerId: string, page: number = 1, limit: number = 100) => {
    state.isNearbyFetching = true

    const { isSuccess, total, status, items, errorMessage } = await retrieveNearbyPassengers({ buildingId, startTime, endTime, passengerId, page, limit })

    if (isSuccess) {
      state.total = total
      state.status = status
      state.nearby = items
    } else {
      state.errorFetchNearby = errorMessage
    }

    state.isNearbyFetching = false
  }

  /**
   * Export
   */
  return {
    ...toRefs(state),
    isNearbyProcessingStatus,
    isNearbyCompletedStatus,
    fetchNearby,
  }
}
