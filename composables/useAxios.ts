import { useContext } from '@nuxtjs/composition-api'

/**
 * Interfaces
 */
export interface IAxios {
  get: Function
  patch: Function
  post: Function
  delete: Function
}

const useAxios = function (): IAxios {
  /**
   * Setup environment
   */
  const context = useContext()

  /**
   * Method: Get
   */
  const get = async (endpoint: string) => {
    const { data } = await context.$axios.get(endpoint)
    return data
  }

  /**
   * Method: Post
   */
  const post = async (endpoint: string, payload: any) => {
    const { data } = await context.$axios.post(endpoint, payload)
    return data
  }

  /**
   * Method: Patch
   */
  const patch = async (endpoint: string, payload: any) => {
    const { data } = await context.$axios.patch(endpoint, payload)
    return data
  }

  /**
   * Method: Remove
   */
  const remove = async (endpoint: string) => {
    const { data } = await context.$axios.delete(endpoint)
    return data
  }

  /**
   * Export
   */
  return {
    get,
    post,
    patch,
    delete: remove,
  }
}

export default useAxios