import { reactive, toRefs } from '@nuxtjs/composition-api'
import { IVideoItem } from '~/contracts'
import retrieveAreaVideos from '~/services/building/retrieveAreaVideos'

export default function () {
  /**
   * Reactive state
   */
  const state = reactive({
    isVideoFetching: false as boolean,
    videos: [] as IVideoItem[],
    errorVideoFetching: undefined as string | undefined
  })

  /**
   * Method: Fetch channels
   */
  const fetchVideos = async (cameraId: string, startTime: string, endTime: string) => {
    state.isVideoFetching = true

    const { isSuccess, items, errorMessage } = await retrieveAreaVideos({ cameraId, startTime, endTime })

    if (isSuccess && items) {
      state.videos = items
    } else {
      state.errorVideoFetching = errorMessage
    }

    state.isVideoFetching = false
  }

  /**
   * Export
   */
  return {
    ...toRefs(state),
    fetchVideos,
  }
}
