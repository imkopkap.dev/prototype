import { reactive, toRefs } from '@nuxtjs/composition-api'
import { IVideoItem } from '~/contracts'
import retrievePersonVideos from '~/services/building/retrievePersonVideos'

export default function () {
  /**
   * Reactive state
   */
  const state = reactive({
    isVideoFetching: true as boolean,
    videos: [] as IVideoItem[],
    errorVideoFetching: undefined as string | undefined
  })

  /**
   * Method: Fetch channels
   */
  const fetchVideos = async (passengerId: string, cameraId: string, startTime: string, endTime: string) => {
    state.isVideoFetching = true

    const { isSuccess, items, errorMessage } = await retrievePersonVideos({ passengerId, cameraId, startTime, endTime })

    if (isSuccess && items) {
      state.videos = items
    } else {
      state.errorVideoFetching = errorMessage
    }

    state.isVideoFetching = false
  }

  /**
   * Export
   */
  return {
    ...toRefs(state),
    fetchVideos,
  }
}
