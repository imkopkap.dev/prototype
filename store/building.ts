import { defineStore } from 'pinia'
import moment from 'moment'
import { IFloorItem, IAreaItem, IUnitItem, IPassengerItem, IPassengerHistoryItem, IDailyItem, IFloorPopulationSummaryItem, IFloorTemperatureItem, IAreaTemperatureItem, IUnitTemperatureItem } from '~/contracts'
import { getAllPeopleHelper, getAllAreaHelper, getAllUnitHelper, getPeopleInAreaHelper, getPeopleInFloorHelper, getPeopleInUnitHelper, getAreaHelper, getAreaByNameHelper, getFloorHelper, getPassengerHelper, searchKeyword } from '~/helpers/building'
import { groupPeopleByHour } from '~/helpers/people'
import retrievePassengers from '~/services/building/retrievePassengers'

const today = moment()

export const useBuilding = defineStore('building', {
  /* Defining the state of the store. */
  state: () => ({
    buildingId: 'ca8ceb5a0a8c48a79e1ab315dfad97ce',
    isBuildingLoading: false as boolean,
    fetchFloorsErrorMessage: undefined as string | undefined,
    floors: [] as IFloorItem[],
    selectedFloorId: undefined as string | undefined,
    selectedAreaId: undefined as string | undefined,
    selectedUnitId: undefined as string | undefined,
    selectedPassengerId: undefined as string | undefined,
    currentDate: today.format('yyyy-MM-DD') as number | string | null,
    currentStartTime: today.format('HH:mm:ss') as string | null,
    currentEndTime: today.format('HH:mm:ss') as string | null,
    lastSelectedStartTime: null as string | null,
    lastSelectedEndTime: null as string | null,
    lastSelectedDate: null as number | string | null,
    searchKeyword: '' as string,
  }),
  /* Defining an action that can be called by the store. */
  actions: {
    /**
     * It retrieves the floors for the building.
     */
    async fetchFloors(currentDate: string, currentStartTime: string, currentEndTime: string, isSetLastSelectedDate: boolean = true) {
      this.isBuildingLoading = true

      if (isSetLastSelectedDate) {
        this.lastSelectedDate = this.currentDate
        this.lastSelectedStartTime = this.currentStartTime
        this.lastSelectedEndTime = this.currentEndTime
      }

      this.currentDate = currentDate
      this.currentStartTime = currentStartTime
      this.currentEndTime = currentEndTime

      /**
       * Parameter
       */
      const startTime = `${this.currentDate} ${this.currentStartTime}`
      const endTime = `${this.currentDate} ${this.currentEndTime}`

      const { isSuccess, errorMessage, item } = await retrievePassengers({ buildingId: this.buildingId, startTime, endTime })
      this.isBuildingLoading = false
    
      if (isSuccess) {
        this.floors = item ? item.floors : [] as IFloorItem[]
        this.selectedFloorId = this.floors[0].id
      } else {
        this.fetchFloorsErrorMessage = errorMessage
      }
    },
   /**
    * This function is called when the user clicks on a floor in the floor list.
    * @param {string} floorId - The id of the floor that was selected.
    */
    onSelectFloor(floorId: string) {
      this.selectedFloorId = floorId
    },
   /**
    * This function is called when the user clicks on a area in the area list.
    * @param {string} areaId - The id of the area that was selected.
    */
    onSelectArea(areaId: string) {
      this.selectedAreaId = areaId
    },
   /**
    * This function is called when the user clicks on unit in the area list.
    * @param {string} unitId - The id of the unit that was selected.
    */
    onSelectUnit(unitId: string) {
      this.selectedUnitId = unitId
    },
   /**
    * This function is called when the user clicks on passenger in the unit list.
    * @param {string} passengerId - The id of the passenger that was selected.
    */
    onSelectPassenger(passengerId: string) {
      this.selectedPassengerId = passengerId
    },
   /**
    * It searches for the keyword in the ID of the passenger, area and unit.
    * @param {string} keyword - string
    */
    search(keyword: string) {
      this.searchKeyword = keyword
    }
  },
  /* Defining a getter. */
  getters: {
    /* This is a getter. It is a function that returns a value. */
    getTotalPopulation(state) {
      let total: number = 0

      for (const floor of state.floors) {
        total += floor.totalPassengers
      }

      return total
    },
    /* This is a getter. It is a function that returns a value. */
    getFloors(state) {
      return state.floors.map(floor => floor)
    },
    /* This is a getter. It is a function that returns a value. */
    getAreas(state): Function {
      return (floorId: string): IAreaItem[] => {
        const floor = getFloorHelper(state.floors, floorId)
        return floor ? floor.areas : []
      }
    },
    /* This is a getter. It is a function that returns a value. */
    getUnits(state): Function {
      return (areaId: string): IUnitItem[] => {
        const area = getAreaHelper(state.floors, areaId)
        return area ? area.units : [] as IUnitItem[]
      }
    },
    /* This is a getter. It is a function that returns a value. */
    getUnitsByAreaName(state): Function {
      return (areaName: string): IUnitItem[] => {
        const area = getAreaByNameHelper(state.floors, areaName)
        return area ? area.units : [] as IUnitItem[]
      }
    },
    /* This is a getter. It is a function that returns a value. */
    getTotalPeopleInFloor(state): Function {
      return (floorId: string): number => {
        const floor = getPeopleInFloorHelper(state.floors, floorId)
        return floor.length
      }
    },
    /* This is a getter. It is a function that returns a value. */
    getTotalPeopleInArea(state): Function {
      return (areaId: string): number => {
        const passengers = getPeopleInAreaHelper(state.floors, areaId)
        return passengers.length
      }
    },
    /* This is a getter. It is a function that returns a value. */
    getPeopleInArea(state): Function {
      return (areaId: string, keyword: string | null): IPassengerItem[] => {
        let passengers = getPeopleInAreaHelper(state.floors, areaId)

        if (keyword) {
          passengers = searchKeyword(passengers, keyword)
        }

        return passengers
      }
    },
    /* This is a getter. It is a function that returns a value. */
    getPeopleInAreaByDaily(state): Function {
      return (areaId: string): IDailyItem[] => {
        const passengers = getPeopleInAreaHelper(state.floors, areaId)
        return groupPeopleByHour(passengers)
      }
    },
    /* This is a getter. It is a function that returns a value. */
    getPeopleInUnit(state): Function {
      return (unitId: string, keyword: string): IPassengerItem[] => {
        let passengers = getPeopleInUnitHelper(state.floors, unitId)

        if (keyword) {
          passengers = searchKeyword(passengers, keyword)
        }

        return passengers
      }
    },
    /* This is a getter. It is a function that returns a value. */
    getPeopleInUnitByDaily(state): Function {
      return (unitId: string): IDailyItem[] => {
        const passengers = getPeopleInUnitHelper(state.floors, unitId)
        return groupPeopleByHour(passengers)
      }
    },
    /* This is a getter. It is a function that returns a value. */
    getPassenger(state): Function {
      return (passengerId: string): IPassengerItem | null => {
        const passenger = getPassengerHelper(state.floors, passengerId)
        return passenger ?? null
      }
    },
    /* This is a getter. It is a function that returns a value. */
    getPersonHistoryById(state): Function {
      return (passengerId: string): IPassengerHistoryItem[] => {
        const passenger = getPassengerHelper(state.floors, passengerId)
        return passenger ? passenger.histories : []
      }
    },
    /* This is a getter. It is a function that returns a value. */
    getFloorPopulationSummary(state): IFloorPopulationSummaryItem[] {
      let total: number = 0

      for (const floor of state.floors) {
        total += floor.totalPassengers
      }

      return state.floors.map(floor => ({
        name: floor.name,
        percentage: floor.totalPassengers ? floor.totalPassengers / total * 100 : 0,
        totalPassengers: floor.totalPassengers
      }))
    },
    /* This is a getter. It is a function that returns a value. */
    getFloorTemperatures(state): IFloorTemperatureItem[] {
      const floors: IFloorTemperatureItem[] = []

      for(const floor of state.floors) {
        const areas: IAreaTemperatureItem[] = []

        for (const area of floor.areas) {
          const units: IUnitTemperatureItem[] = []

          for (const unit of area.units) {
            const passengers: IPassengerItem[] = []

            for (const passenger of unit.passengers) {
              if (passenger.temperature > 37.5) {
                passengers.push(passenger)
              }
            }

            units.push({
              id: unit.id,
              name: unit.name, 
              total: passengers.length,
              passengers,
            })
          }

          areas.push({
            id: area.id,
            name: area.name,
            total: units.reduce((a, b) => a + b.total, 0),
            units,
          })
        }

        floors.push({
          id: floor.id,
          name: floor.name,
          total: areas.reduce((a, b) => a + b.total, 0),
          areas,
        })
      }

      return floors 
    },
    /* This is a getter. It is a function that returns a value. */
    gePeopleInAreaTemperatures(state): Function {
      return (areaId: string, keyword: string | null): IPassengerItem[] => {
        let passengers = getPeopleInAreaHelper(state.floors, areaId)
        passengers = passengers.filter(passenger => passenger.temperature > 37.5)

        if (keyword) {
          passengers = searchKeyword(passengers, keyword)
        }

        return passengers
      }
    },
    searchPassengersByKeyword(state): IPassengerItem[] {
      const passengers = getAllPeopleHelper(state.floors)
      return passengers.filter((passenger: IPassengerItem) => passenger.displayName.includes(state.searchKeyword))
    },
    searchAreasByKeyword(state): IAreaItem[] {
      const areas = getAllAreaHelper(state.floors)
      return areas.filter((area: IAreaItem) => area.name.includes(state.searchKeyword))
    } ,
    searchUnitsByKeyword(state): IUnitItem[] {
      const units = getAllUnitHelper(state.floors)
      return units.filter((unit: IUnitItem) => unit.name.includes(state.searchKeyword))
    },
    areaCameraIds(state): Function {
      return (areaId: string): string[] => {
        const area = getAreaHelper(state.floors, areaId)
        return area ? area.cameraIds : []
      }
    }, 
  },
})