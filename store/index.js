import building from '~/modules/building/store/index'
import floor from '~/modules/floor/store/index'
import zone from '~/modules/zone/store/index'
import unit from '~/modules/unit/store/index'
import people from '~/modules/people/store/index'
import search from '~/modules/search/store/index'

export const modules = {
  building,
  floor,
  zone,
  unit,
  people,
  search,
}
